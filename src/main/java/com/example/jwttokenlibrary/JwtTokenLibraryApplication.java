package com.example.jwttokenlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtTokenLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtTokenLibraryApplication.class, args);
    }

}
